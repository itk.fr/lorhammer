package provisioning

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"io/ioutil"
	"lorhammer/src/model"
	"net"
	"net/http"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

var logLoraserver = logrus.WithField("logger", "orchestrator/provisioning/loraserver")

const (
	loraserverType             = Type("loraserver")
	loraserverOrganisationName = "lorhammer"
	loraserverApplicationName  = "Lorhammer"
	httpLoraserverTimeout      = 1 * time.Minute
)

type httpClientSender interface {
	Do(*http.Request) (*http.Response, error)
}

type loraserver struct {
	APIURL                string `json:"apiUrl"`
	Login                 string `json:"login"`
	Password              string `json:"password"`
	jwtToKen              string
	OrganizationID        string `json:"organizationId"`
	NetworkServerID       string `json:"networkServerId"`
	NetworkServerAddr     string `json:"networkServerAddr"`
	ServiceProfileID      string `json:"serviceProfileID"`
	AppID                 string `json:"appId"`
	DeviceProfileID       string `json:"deviceProfileId"`
	Abp                   bool   `json:"abp"`
	NbProvisionerParallel int    `json:"nbProvisionerParallel"`
	DeleteOrganization    bool   `json:"deleteOrganization"`
	DeleteApplication     bool   `json:"deleteApplication"`

	provisionedDeviceProfileID  string
	provisionedGateways         []model.Gateway
	provisionedServiceProfileID string
	httpClient                  httpClientSender
	mu                          sync.RWMutex
}

// Thread safe getters and setters.
// 'Get' prefix added to fields with JSON tag due to unmarshalling requiring exported fields.
func (loraserver *loraserver) GetAppID() string {
	loraserver.mu.RLock()
	defer loraserver.mu.RUnlock()
	return loraserver.AppID
}

func (loraserver *loraserver) SetAppID(newAppID string) {
	loraserver.mu.Lock()
	defer loraserver.mu.Unlock()
	loraserver.AppID = newAppID
}

func (loraserver *loraserver) ProvisionedDeviceProfileID() string {
	loraserver.mu.RLock()
	defer loraserver.mu.RUnlock()
	return loraserver.provisionedDeviceProfileID
}

func (loraserver *loraserver) SetProvisionedDeviceProfileID(dp string) {
	loraserver.mu.Lock()
	defer loraserver.mu.Unlock()
	loraserver.provisionedDeviceProfileID = dp
}

func (loraserver *loraserver) ProvisionedGateways() []model.Gateway {
	loraserver.mu.RLock()
	defer loraserver.mu.RUnlock()
	return loraserver.provisionedGateways
}

func (loraserver *loraserver) SetProvisionedGateways(newGateways []model.Gateway) {
	loraserver.mu.Lock()
	defer loraserver.mu.Unlock()
	loraserver.provisionedGateways = newGateways
}

func (loraserver *loraserver) ProvisionedServiceProfileID() string {
	loraserver.mu.RLock()
	defer loraserver.mu.RUnlock()
	return loraserver.provisionedServiceProfileID
}

func (loraserver *loraserver) SetProvisionedServiceProfileID(sp string) {
	loraserver.mu.Lock()
	defer loraserver.mu.Unlock()
	loraserver.provisionedServiceProfileID = sp
}

func (loraserver *loraserver) GetOrganizationID() string {
	loraserver.mu.RLock()
	defer loraserver.mu.RUnlock()
	return loraserver.OrganizationID
}

func (loraserver *loraserver) SetOrganizationID(newOrgID string) {
	loraserver.mu.Lock()
	defer loraserver.mu.Unlock()
	loraserver.OrganizationID = newOrgID
}

func newLoraserver(rawConfig json.RawMessage) (provisioner, error) {
	config := &loraserver{
		httpClient: &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
				Dial: (&net.Dialer{
					Timeout: httpLoraserverTimeout,
				}).Dial,
				TLSHandshakeTimeout: httpLoraserverTimeout,
			},
			Timeout: httpLoraserverTimeout,
		},
	}
	if err := json.Unmarshal(rawConfig, config); err != nil {
		return nil, err
	}

	return config, nil
}

func (loraserver *loraserver) Provision(reg model.Register) error {
	if loraserver.jwtToKen == "" {
		req := struct {
			Login    string `json:"username"`
			Password string `json:"password"`
		}{
			Login:    loraserver.Login,
			Password: loraserver.Password,
		}

		resp := struct {
			Jwt string `json:"jwt"`
		}{}

		err := loraserver.doRequest(loraserver.APIURL+"/api/internal/login", "POST", req, &resp)
		if err != nil {
			return err
		}

		loraserver.jwtToKen = resp.Jwt
	}

	if err := loraserver.initOrganizationID(); err != nil {
		return err
	}

	if err := loraserver.initNetworkServer(); err != nil {
		return err
	}

	if err := loraserver.initServiceProfile(); err != nil {
		return err
	}

	if err := loraserver.initApplication(); err != nil {
		return err
	}

	if err := loraserver.initDeviceProfile(); err != nil {
		return err
	}

	//Provision Gateways
	numGateways := len(reg.Gateways)
	gatewayChan := make(chan model.Gateway, numGateways)
	gatewayErrorChan := make(chan error, numGateways)
	defer close(gatewayErrorChan)
	gatewayFinishChan := make(chan model.Gateway, numGateways)
	defer close(gatewayFinishChan)

	var wg sync.WaitGroup
	wg.Add(loraserver.NbProvisionerParallel)

	for i := 0; i < loraserver.NbProvisionerParallel; i++ {
		go loraserver.provisionGatewayAsync(gatewayChan, gatewayErrorChan, gatewayFinishChan, &wg)
	}

	go func() {
		for _, gateway := range reg.Gateways {
			gatewayChan <- gateway
		}
		close(gatewayChan)
	}()

	var hasErr bool
	for i := 0; i < numGateways; i++ {
		select {
		case <-gatewayErrorChan:
			hasErr = true
		case gateway := <-gatewayFinishChan:
			logLoraserver.WithField("gateway", gateway).Debug("Gateway", gateway.MacAddress.String(), "provisioned")
			loraserver.SetProvisionedGateways(append(loraserver.provisionedGateways, gateway))
		}
	}

	wg.Wait()

	if hasErr {
		return errors.New("error provisioning gateways")
	}

	//Provision sensors
	nbNodeToProvision := 0
	for _, gateway := range reg.Gateways {
		for range gateway.Nodes {
			nbNodeToProvision++
		}
	}
	nodeChan := make(chan *model.Node, nbNodeToProvision)
	nodeErrorChan := make(chan error, nbNodeToProvision)
	defer close(nodeErrorChan)
	nodeFinishChan := make(chan *model.Node, nbNodeToProvision)
	defer close(nodeFinishChan)

	wg.Add(loraserver.NbProvisionerParallel)

	for i := 0; i < loraserver.NbProvisionerParallel; i++ {
		go loraserver.provisionNodeAsync(nodeChan, nodeErrorChan, nodeFinishChan, &wg)
	}

	go func() {
		for _, gateway := range reg.Gateways {
			gateway := gateway
			for _, node := range gateway.Nodes {
				node := node
				nodeChan <- node
			}
		}
		close(nodeChan)
	}()

	for i := 0; i < nbNodeToProvision; i++ {
		select {
		case err := <-nodeErrorChan:
			logLoraserver.WithError(err).Error("Node not provisioned")
		case node := <-nodeFinishChan:
			logLoraserver.WithField("node", node).Debug("Node provisioned")
		}
	}

	wg.Wait()

	return nil
}

func (loraserver *loraserver) initOrganizationID() error {
	if loraserver.GetOrganizationID() == "" {
		// Check if already exist
		type Organization struct {
			ID   string `json:"id"`
			Name string `json:"name"`
		}

		respExist := struct {
			Result []Organization `json:"result"`
		}{}

		err := loraserver.doRequest(loraserver.APIURL+"/api/organizations?limit=100", "GET", nil, &respExist)
		if err != nil {
			return err
		}
		for _, orga := range respExist.Result {
			if orga.Name == loraserverOrganisationName {
				loraserver.SetOrganizationID(orga.ID)
				break
			}
		}

		// Create Organization
		if loraserver.GetOrganizationID() == "" {
			req := struct {
				Organization struct {
					CanHaveGateways bool   `json:"canHaveGateways"`
					DisplayName     string `json:"displayName"`
					Name            string `json:"name"`
				} `json:"organization"`
			}{
				Organization: struct {
					CanHaveGateways bool   `json:"canHaveGateways"`
					DisplayName     string `json:"displayName"`
					Name            string `json:"name"`
				}{
					CanHaveGateways: true,
					DisplayName:     "Lorhammer",
					Name:            loraserverOrganisationName,
				},
			}

			resp := struct {
				ID string `json:"id"`
			}{}

			err := loraserver.doRequest(loraserver.APIURL+"/api/organizations", "POST", req, &resp)
			if err != nil {
				return err
			}
			loraserver.SetOrganizationID(resp.ID)
		}
	}
	return nil
}

func (loraserver *loraserver) initNetworkServer() error {
	if loraserver.NetworkServerID == "" {
		// Check if already exist
		type NetworkServer struct {
			ID     string `json:"id"`
			Server string `json:"server"`
		}

		respExist := struct {
			Result []NetworkServer `json:"result"`
		}{}

		err := loraserver.doRequest(loraserver.APIURL+"/api/network-servers?limit=100", "GET", nil, &respExist)
		if err != nil {
			return err
		}
		for _, ns := range respExist.Result {
			if ns.Server == loraserver.NetworkServerAddr {
				loraserver.NetworkServerID = ns.ID
				break
			}
		}
		// Create NS
		if loraserver.NetworkServerID == "" {
			req := struct {
				NetworkServer struct {
					Server string `json:"server"`
					Name   string `json:"name"`
				} `json:"networkServer"`
			}{
				NetworkServer: struct {
					Server string `json:"server"`
					Name   string `json:"name"`
				}{
					Server: loraserver.NetworkServerAddr,
					Name:   loraserver.NetworkServerAddr,
				},
			}

			resp := struct {
				ID string `json:"id"`
			}{}

			err = loraserver.doRequest(loraserver.APIURL+"/api/network-servers", "POST", req, &resp)
			if err != nil {
				return err
			}

			loraserver.NetworkServerID = resp.ID
		}
	}
	return nil
}

func (loraserver *loraserver) initServiceProfile() error {
	if loraserver.ServiceProfileID == "" {
		req := struct {
			ServiceProfile struct {
				Name            string `json:"name"`
				NetworkServerID string `json:"networkServerID"`
				OrganizationID  string `json:"organizationID"`
				AddGWMetadata   bool   `json:"addGWMetadata"`
				// ChannelMask            string `json:"channelMask"`
				// DevStatusReqFreq       int    `json:"devStatusReqFreq"`
				// DlBucketSize           int    `json:"dlBucketSize"`
				// DlRate                 int    `json:"dlRate"`
				// DlRatePolicy           string `json:"dlRatePolicy"`
				// DrMax                  int    `json:"drMax"`
				// DrMin                  int    `json:"drMin"`
				// HrAllowed              bool   `json:"hrAllowed"`
				// MinGWDiversity         int    `json:"minGWDiversity"`
				// NwkGeoLoc              bool   `json:"nwkGeoLoc"`
				// PrAllowed              bool   `json:"prAllowed"`
				// RaAllowed              bool   `json:"raAllowed"`
				// ReportDevStatusBattery bool   `json:"reportDevStatusBattery"`
				// ReportDevStatusMargin  bool   `json:"reportDevStatusMargin"`
				// ServiceProfileID       string `json:"serviceProfileID"`
				// TargetPER              int    `json:"targetPER"`
				// UlBucketSize           int    `json:"ulBucketSize"`
				// UlRate                 int    `json:"ulRate"`
				// UlRatePolicy           string `json:"ulRatePolicy"`
				// TODO find description and meaning of all fields
			} `json:"serviceProfile"`
		}{
			ServiceProfile: struct {
				Name            string `json:"name"`
				NetworkServerID string `json:"networkServerID"`
				OrganizationID  string `json:"organizationID"`
				AddGWMetadata   bool   `json:"addGWMetadata"`
			}{
				Name:            "LorhammerServiceProfile",
				NetworkServerID: loraserver.NetworkServerID,
				OrganizationID:  loraserver.GetOrganizationID(),
				AddGWMetadata:   true,
			},
		}

		resp := struct {
			ID string `json:"id"`
		}{}

		err := loraserver.doRequest(loraserver.APIURL+"/api/service-profiles", "POST", req, &resp)
		if err != nil {
			return err
		}

		loraserver.ServiceProfileID = resp.ID
		loraserver.SetProvisionedServiceProfileID(resp.ID)
	}
	return nil
}

func (loraserver *loraserver) initApplication() error {
	if loraserver.GetAppID() == "" {
		// Check if already exist
		type Application struct {
			ID   string `json:"id"`
			Name string `json:"name"`
		}

		respExist := struct {
			Result []Application `json:"result"`
		}{}

		err := loraserver.doRequest(loraserver.APIURL+"/api/applications?limit=100", "GET", nil, &respExist)
		if err != nil {
			return err
		}
		for _, app := range respExist.Result {
			if app.Name == loraserverApplicationName {
				loraserver.SetAppID(app.ID)
				break
			}
		}

		// Create Application
		if loraserver.GetAppID() == "" {
			req := struct {
				Application struct {
					Name             string      `json:"name"`
					Description      string      `json:"description"`
					OrganizationID   string      `json:"organizationID"`
					ServiceProfileID interface{} `json:"serviceProfileID"`
				} `json:"application"`
			}{
				Application: struct {
					Name             string      `json:"name"`
					Description      string      `json:"description"`
					OrganizationID   string      `json:"organizationID"`
					ServiceProfileID interface{} `json:"serviceProfileID"`
				}{
					Name:             loraserverApplicationName,
					Description:      "Lorhammer",
					OrganizationID:   loraserver.GetOrganizationID(),
					ServiceProfileID: loraserver.ServiceProfileID,
				},
			}

			resp := struct {
				ID string `json:"id"`
			}{}

			err := loraserver.doRequest(loraserver.APIURL+"/api/applications", "POST", req, &resp)
			if err != nil {
				return err
			}
			loraserver.SetAppID(resp.ID)
		}
	}
	return nil
}

func (loraserver *loraserver) initDeviceProfile() error {
	if loraserver.DeviceProfileID == "" {
		req := struct {
			DeviceProfile struct {
				Name            string `json:"name"`
				OrganizationID  string `json:"organizationID"`
				NetworkServerID string `json:"networkServerID"`
				RxDROffset1     int    `json:"rxDROffset1"`
				RxDataRate2     int    `json:"rxDataRate2"`
				RxDelay1        int    `json:"rxDelay1"`
				// ClassBTimeout           int    `json:"classBTimeout"`
				// ClassCTimeout           int    `json:"classCTimeout"`
				// FactoryPresetFreqs      []int  `json:"factoryPresetFreqs"`
				// MacVersion              string `json:"macVersion"`
				// MaxDutyCycle            int    `json:"maxDutyCycle"`
				// MaxEIRP                 int    `json:"maxEIRP"`
				// PingSlotDR              int    `json:"pingSlotDR"`
				// PingSlotFreq            int    `json:"pingSlotFreq"`
				// PingSlotPeriod          int    `json:"pingSlotPeriod"`
				// RegParamsRevisionstring string `json:"regParamsRevisionstring"`
				// RfRegionstring          string `json:"rfRegionstring"`
				// RxFreq2           int  `json:"rxFreq2"`
				// Supports32bitFCnt bool `json:"supports32bitFCnt"`
				// SupportsClassB    bool `json:"supportsClassB"`
				// SupportsClassC    bool `json:"supportsClassC"`
				// SupportsJoin      bool `json:"supportsJoin"`
				// TODO find description and meaning of all fields
			} `json:"deviceProfile"`
		}{
			DeviceProfile: struct {
				Name            string `json:"name"`
				OrganizationID  string `json:"organizationID"`
				NetworkServerID string `json:"networkServerID"`
				RxDROffset1     int    `json:"rxDROffset1"`
				RxDataRate2     int    `json:"rxDataRate2"`
				RxDelay1        int    `json:"rxDelay1"`
			}{
				Name:            "LorhammerDeviceProfile",
				OrganizationID:  loraserver.GetOrganizationID(),
				NetworkServerID: loraserver.NetworkServerID,
				RxDROffset1:     0,
				RxDataRate2:     0,
				RxDelay1:        0,
			},
		}

		resp := struct {
			ID string `json:"id"`
		}{}

		err := loraserver.doRequest(loraserver.APIURL+"/api/device-profiles", "POST", req, &resp)
		if err != nil {
			return err
		}

		loraserver.DeviceProfileID = resp.ID
		loraserver.SetProvisionedDeviceProfileID(resp.ID)
	}
	return nil
}

func (loraserver *loraserver) provisionGatewayAsync(gatewayChan <-chan model.Gateway, errorChan chan<- error, gatewayFinishChan chan<- model.Gateway, wg *sync.WaitGroup) {
	for gateway := range gatewayChan {
		type Location struct {
			Source string `json:"source"`
		}
		type Gateway struct {
			Name            string   `json:"name"`
			ID              string   `json:"id"`
			NetworkServerID string   `json:"networkServerID"`
			OrganizationID  string   `json:"organizationID"`
			Location        Location `json:"location"`
		}
		req := struct {
			Gateway Gateway `json:"gateway"`
		}{
			Gateway{
				Name:            gateway.MacAddress.String(),
				ID:              gateway.MacAddress.String(),
				NetworkServerID: loraserver.NetworkServerID,
				OrganizationID:  loraserver.GetOrganizationID(),
				Location: Location{
					Source: "UNKNOWN",
				},
			},
		}

		err := loraserver.doRequest(loraserver.APIURL+"/api/gateways", "POST", req, nil)
		if err != nil {
			logLoraserver.WithField("req", req).WithError(err).Error("Can't register gateway")
			errorChan <- err
			continue
		}
		gatewayFinishChan <- gateway
	}
	wg.Done()
}

func (loraserver *loraserver) provisionNodeAsync(nodeChan <-chan *model.Node, errorChan chan<- error, nodeFinishChan chan<- *model.Node, wg *sync.WaitGroup) {
	for node := range nodeChan {
		if node != nil { // Why sensor is nil sometimes !?
			req := struct {
				Device struct {
					Name            string `json:"name"`
					Description     string `json:"description"`
					ApplicationID   string `json:"applicationID"`
					DeviceProfileID string `json:"deviceProfileID"`
					DevEUI          string `json:"devEUI"`
				} `json:"device"`
			}{
				Device: struct {
					Name            string `json:"name"`
					Description     string `json:"description"`
					ApplicationID   string `json:"applicationID"`
					DeviceProfileID string `json:"deviceProfileID"`
					DevEUI          string `json:"devEUI"`
				}{
					Name:            "STRESSNODE_" + node.DevEUI.String(),
					Description:     node.Description,
					ApplicationID:   loraserver.GetAppID(),
					DeviceProfileID: loraserver.DeviceProfileID,
					DevEUI:          node.DevEUI.String(),
				},
			}
			if req.Device.Description == "" { // device description is required
				req.Device.Description = req.Device.Name
			}

			err := loraserver.doRequest(loraserver.APIURL+"/api/devices", "POST", req, nil)
			if err != nil {
				logLoraserver.WithField("req", req).WithError(err).Error("Can't register node")
				errorChan <- err
				continue
			}

			reqKeys := struct {
				DeviceKeys struct {
					DevEUI string `json:"devEUI"`
					AppKey string `json:"appKey"`
					NwkKey string `json:"nwkKey"`
				} `json:"deviceKeys"`
			}{
				DeviceKeys: struct {
					DevEUI string `json:"devEUI"`
					AppKey string `json:"appKey"`
					NwkKey string `json:"nwkKey"`
				}{
					DevEUI: node.DevEUI.String(),
					AppKey: node.AppKey.String(),
					NwkKey: node.AppKey.String(),
				},
			}

			err = loraserver.doRequest(loraserver.APIURL+"/api/devices/"+node.DevEUI.String()+"/keys", "POST", reqKeys, nil)
			if err != nil {
				logLoraserver.WithField("reqKeys", reqKeys).WithError(err).Error("Can't register keys device")
				errorChan <- err
				continue
			}

			if loraserver.Abp {
				req := struct {
					DeviceActivation struct {
						AppSKeystring string `json:"appSKey"`
						DevAddrstring string `json:"devAddr"`
						DevEUIstring  string `json:"devEUI"`
						FCntDown      int    `json:"fCntDown"`
						FCntUp        int    `json:"fCntUp"`
						NwkSKeystring string `json:"nwkSKey"`
						NwkSEncKey    string `json:"nwkSEncKey"`
						SNwkSEncKey   string `json:"sNwkSIntKey"`
						FNwkSEncKey   string `json:"fNwkSIntKey"`
						SkipFCntCheck bool   `json:"skipFCntCheck"`
					} `json:"deviceActivation"`
				}{
					DeviceActivation: struct {
						AppSKeystring string `json:"appSKey"`
						DevAddrstring string `json:"devAddr"`
						DevEUIstring  string `json:"devEUI"`
						FCntDown      int    `json:"fCntDown"`
						FCntUp        int    `json:"fCntUp"`
						NwkSKeystring string `json:"nwkSKey"`
						NwkSEncKey    string `json:"nwkSEncKey"`
						SNwkSEncKey   string `json:"sNwkSIntKey"`
						FNwkSEncKey   string `json:"fNwkSIntKey"`
						SkipFCntCheck bool   `json:"skipFCntCheck"`
					}{
						AppSKeystring: node.AppSKey.String(),
						DevAddrstring: node.DevAddr.String(),
						DevEUIstring:  node.DevEUI.String(),
						FCntDown:      0,
						FCntUp:        0,
						NwkSKeystring: node.NwSKey.String(),
						NwkSEncKey:    node.NwSKey.String(),
						SNwkSEncKey:   node.NwSKey.String(),
						FNwkSEncKey:   node.NwSKey.String(),
						SkipFCntCheck: false,
					},
				}
				url := loraserver.APIURL + "/api/devices/" + node.DevEUI.String() + "/activate"
				err := loraserver.doRequest(url, "POST", req, nil)
				if err != nil {
					logLoraserver.WithError(err).Error("Can't activate abp device")
					errorChan <- err
					continue
				}
			}
			nodeFinishChan <- node
		}
	}
	wg.Done()
}

func (loraserver *loraserver) DeProvision() error {
	nodes := make([]*model.Node, 0)
	for _, gateway := range loraserver.ProvisionedGateways() {
		nodes = append(nodes, gateway.Nodes...)
	}

	var hasErr bool
	if err := loraserver.deProvisionNodes(nodes); err != nil {
		hasErr = true
	}

	if err := loraserver.deProvisionGateways(loraserver.provisionedGateways); err != nil {
		hasErr = true
	}

	appID := loraserver.GetAppID()
	if loraserver.DeleteApplication && appID != "" {
		if err := loraserver.doRequest(loraserver.APIURL+"/api/applications/"+appID, "DELETE", nil, nil); err != nil {
			logrus.WithError(err).Error("Can't de-provision application", appID)
			hasErr = true
		}
	}

	dp := loraserver.ProvisionedDeviceProfileID()
	if loraserver.ProvisionedDeviceProfileID() != "" {
		if err := loraserver.doRequest(loraserver.APIURL+"/api/device-profiles/"+dp, "DELETE", nil, nil); err != nil {
			logrus.WithError(err).Error("Can't de-provision device profile", dp)
			hasErr = true
		}
	}

	sp := loraserver.ProvisionedServiceProfileID()
	if loraserver.ProvisionedServiceProfileID() != "" {
		if err := loraserver.doRequest(loraserver.APIURL+"/api/service-profiles/"+sp, "DELETE", nil, nil); err != nil {
			logrus.WithError(err).Error("Can't de-provision service profile", sp)
			hasErr = true
		}
	}

	orgID := loraserver.GetOrganizationID()
	if loraserver.DeleteOrganization && orgID != "" {
		if err := loraserver.doRequest(loraserver.APIURL+"/api/organizations/"+orgID, "DELETE", nil, nil); err != nil {
			logrus.WithError(err).Error("Can't de-provision organization", orgID)
			hasErr = true
		}
	}

	if hasErr {
		return errors.New("error(s) occurred while de-provisioning")
	}

	return nil
}

func (loraserver *loraserver) deProvisionNodes(nodes []*model.Node) error {
	nbNodes := len(nodes)
	nodeChan := make(chan model.Node, nbNodes)
	finishChan := make(chan model.Node, nbNodes)
	defer close(finishChan)
	errorChan := make(chan error, nbNodes)
	defer close(errorChan)

	var wg sync.WaitGroup
	wg.Add(loraserver.NbProvisionerParallel)

	for i := 0; i < loraserver.NbProvisionerParallel; i++ {
		go func() {
			for n := range nodeChan {
				if err := loraserver.doRequest(loraserver.APIURL+"/api/devices/"+n.DevEUI.String(), "DELETE", nil, nil); err != nil {
					errorChan <- err
					break
				}
				finishChan <- n
			}
			wg.Done()
		}()
	}

	go func() {
		for _, n := range nodes {
			nodeChan <- *n
		}
		close(nodeChan)
	}()

	hasErr := false
	for i := 0; i < nbNodes; i++ {
		select {
		case err := <-errorChan:
			logLoraserver.WithError(err).Error("Node not de-provisioned")
			hasErr = true
		case node := <-finishChan:
			logLoraserver.WithField("node", node).Debug("Node de-provisioned", node.DevEUI.String())
		}
	}

	wg.Wait()

	if hasErr {
		return errors.New("error de-provisioning nodes")
	}

	return nil
}

func (loraserver *loraserver) deProvisionGateways(gateways []model.Gateway) error {
	nbGateways := len(gateways)
	gatewayChan := make(chan model.Gateway, nbGateways)
	finishChan := make(chan model.Gateway, nbGateways)
	defer close(finishChan)
	errorChan := make(chan error, nbGateways)
	defer close(errorChan)

	var wg sync.WaitGroup
	wg.Add(loraserver.NbProvisionerParallel)

	for i := 0; i < loraserver.NbProvisionerParallel; i++ {
		go func() {
			for g := range gatewayChan {
				if err := loraserver.doRequest(loraserver.APIURL+"/api/gateways/"+g.MacAddress.String(), "DELETE", nil, nil); err != nil {
					errorChan <- err
					break
				}
				finishChan <- g
			}
			wg.Done()
		}()
	}

	go func() {
		for _, g := range gateways {
			gatewayChan <- g
		}
		close(gatewayChan)
	}()

	hasErr := false
	for i := 0; i < len(gateways); i++ {
		select {
		case err := <-errorChan:
			logLoraserver.WithError(err).Error("Gateway not de-provisioned")
			hasErr = true
		case g := <-finishChan:
			logLoraserver.WithField("gateway", g).Debug("Gateway de-provisioned", g.MacAddress.String())
		}
	}

	wg.Wait()

	if hasErr {
		return errors.New("error de-provisioning gateways")
	}

	return nil
}

func (loraserver *loraserver) doRequest(url string, method string, bodyRequest interface{}, bodyResult interface{}) error {
	logLoraserver.WithField("url", url).Debug("Will call")
	ctx, cancelCtx := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelCtx()

	marshalledBodyRequest, err := json.Marshal(bodyRequest)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(method, url, bytes.NewBuffer(marshalledBodyRequest))
	if err != nil {
		return err
	}

	if loraserver.jwtToKen != "" {
		req.Header.Set("Grpc-Metadata-Authorization", loraserver.jwtToKen)
	}
	req.Header.Set("Content-Type", "application/json")
	req.Close = true
	req.WithContext(ctx)

	resp, err := loraserver.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	switch resp.StatusCode {
	case http.StatusOK:
		logLoraserver.WithField("url", url).Debug("Call succeeded")

	default:
		logLoraserver.WithFields(logrus.Fields{
			"respStatus":   resp.StatusCode,
			"responseBody": string(body),
			"requestBody":  string(marshalledBodyRequest),
			"url":          url,
		}).Warn("Couldn't proceed with request")
		return errors.New("Couldn't proceed with request")
	}

	if body != nil && bodyResult != nil {
		return json.Unmarshal(body, bodyResult)
	}
	return nil
}
